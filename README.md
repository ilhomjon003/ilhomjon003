## Hi 👋, I'm Ilhomjon from Uzbekistan.
### Web Developer

### :boom: Skills

[![My Skills](https://skills.thijs.gg/icons?i=html,css,scss,js,ts,react,redux,firebase&theme=light)](https://skills.thijs.gg)
        

- 🤔 I’m looking for help with Typescript 
- 💬 Ask me about **HTML, CSS, JS and REACT** 
- 📫 How to reach me: via [telegram](https://t.me/ilhomjondev) 
### :boom: Contact
 <div style:"display: "inline-block">
   <a href="https://www.linkedin.com/in/ilhomjon-isaqjonov-b0b62b226/" target="_blank"><img src="https://img.shields.io/badge/-LinkedIn-%230077B5?style=for-the-badge&logo=linkedin&logoColor=white" target="_blank"></a> 
   <a href="https://instagram.com/ilhomjon.isaqjonov" target="_blank"><img src="https://img.shields.io/badge/-Instagram-%23E4405F?style=for-the-badge&logo=instagram&logoColor=white" target="_blank"></a>
   <a href = "mailto: ilhomjonisoqjov2@gmail.com"><img src="https://img.shields.io/badge/-Gmail-%23333?style=for-the-badge&logo=gmail&logoColor=white" target="_blank"> </a>
 </br>
</div>
<br/>

### :boom:My Github Stats

<img src="https://github-readme-stats.vercel.app/api?username=ilhomjon003&show_icons=true&theme=dark" width="400">

[![Top Langs](https://github-readme-stats.vercel.app/api/top-langs/?username=ilhomjon003&layout=compact&theme=dark)](https://github.com/anuraghazra/github-readme-stats)

[![GitHub Streak](http://github-readme-streak-stats.herokuapp.com?user=ilhomjon003&theme=dark)](https://git.io/streak-stats) 

![Profile views](https://gpvc.arturio.dev/ilhomjon003)

<a href="https://wakatime.com/@96c624d1-0b41-48bb-b954-a9efd41cfa9d"><img src="https://wakatime.com/badge/user/96c624d1-0b41-48bb-b954-a9efd41cfa9d.svg" alt="Total time coded since Aug 3 2022" /></a>

![Snake animation](https://github.com/ilhomjon003/ilhomjon003/blob/output/github-contribution-grid-snake.svg)
